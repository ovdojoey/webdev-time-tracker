var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var autoprefixer = require('gulp-autoprefixer');
var uglify = require('gulp-uglify');

gulp.task('default', function(){

    gulp.src('./styles/*.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(minifyCss())
        .pipe(gulp.dest('./css'));

    gulp.src('./tracker.js')
        .pipe(uglify())
        .pipe(gulp.dest('./js/'));

});


var watcher = gulp.watch('./styles/*.scss', ['default']);
var watcherJS = gulp.watch('./tracker.js', ['default']);

watcher.on('change', function(event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
});
watcherJS.on('change', function(event) {
    console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
});
