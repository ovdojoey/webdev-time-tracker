// Build Object to store app.
var TIMETRACKER = TIMETRACKER || {};
TIMETRACKER.main = {
  start: function(){

      // load and initialize all the screens and click listeners
      TIMETRACKER.screens.init();

      // load all projects
      TIMETRACKER.projects.init();

      // start timer lookup
      TIMETRACKER.timer.init();

      // start goals
      TIMETRACKER.goals.init();


  }
};
TIMETRACKER.projects = {
    allProjects: null,
    addingProject: false,
    loadedProjectData: null,
    removeVerification: false,
    loadingProject: false,
    projectInit: false,
    init: function(){
        var projectList = document.getElementById('projects');

        //var bytes = chrome.storage.sync.getBytesInUse(null, function(bytesIn){
        //    console.log(bytesIn);
        //});

        /* Load Projects from local or add new blank array  */
        chrome.storage.sync.get("projects", function(obj) {

            // set the storage returned var in the object for use later or set as empty array
            TIMETRACKER.projects.allProjects =  obj.projects || [];

            // clear the existing project list (the project select option list)
            projectList.innerHTML = '';

            // add a basic select starter option
            var optionNew = document.createElement("option");
            optionNew.text = 'Select a Project';
            optionNew.value = '';
            projectList.add(optionNew);

            /* Load projects to option list */
            for(var x = 0; x < TIMETRACKER.projects.allProjects.length; x++)
            {
                var t = TIMETRACKER.projects.allProjects[x];
                var option = document.createElement("option");
                option.text = t.name;
                projectList.add(option);
            }

            optionNew = document.createElement("option");
            optionNew.text = '[Add a Project]';
            optionNew.value = 'new';
            projectList.add(optionNew);

            if(!TIMETRACKER.projects.projectInit) {
                /* Add miniScreen listener for exiting back to project */
                var mSe = document.getElementById("exitAddProject");
                mSe.addEventListener("click", function () {
                    TIMETRACKER.projects.exitAddScreen();
                });

                /* Add miniScreen listener for finalizing addition of project */
                var AddNP = document.getElementById("AddTrackNewProject");
                AddNP.addEventListener("click", function () {
                    TIMETRACKER.projects.execAddNewProject();
                });

                /* Listen for changes */
                projectList.addEventListener("change", function () {
                    if (this.value === 'new') TIMETRACKER.projects.addNew();
                    else if (this.value !== '')  TIMETRACKER.projects.loadProject(this.value);
                    else TIMETRACKER.screens.loadHomeScreen();
                });
                TIMETRACKER.projects.projectInit = true;
            }
        });
    },
    addNew: function(){

        // hide the start screen
        var tb = document.getElementById("trackBoxContainer");
        tb.style.display = "none";

        // hide the project details
        var pDt = document.getElementById("projectDetailsContainer");
        pDt.style.display = "none";

        // show the add screen
        var addS = document.getElementById("addProjectScreen");
        addS.className = " miniScreen active show ";

        // add enter listener
        var pNi = document.getElementById("projectName");
        pNi.addEventListener('keypress', function (e) {
            var key = e.which || e.keyCode;
            if (key === 13) { // 13 is enter
                e.preventDefault();
                TIMETRACKER.projects.execAddNewProject();
            }
        });

    },
    execAddNewProject: function(){
        if(this.addingProject) return false;
        this.addingProject = true;

        function saveProject(name) {
            if (TIMETRACKER.projects.allProjects.length > 20)
            {
                hText.innerText = "Too many projects.  Remove some first.";
                return false;
            }

            for(var i = 0; i < TIMETRACKER.projects.allProjects.length; i++)
            {
                if(TIMETRACKER.projects.allProjects[i].name === name)
                {
                    // Force exit // reset addingProject variable for adding new project
                    TIMETRACKER.projects.addingProject = false;
                    hText.innerText = "You already have a project with the name "+name+".";
                    return false;
                }
            }

            // trigger css animation
            var addNP = document.getElementById("AddTrackNewProject");
            addNP.className = "save";
            pNi.className = "";


            hText.innerText = "Saving project.";

            // save project to local storage and google sync
            TIMETRACKER.projects.allProjects.push( { name: name, totalHours : 0 } );
            var newId = TIMETRACKER.projects.allProjects.length;
            chrome.storage.sync.set({"projects" : TIMETRACKER.projects.allProjects}, function(){

                // clear add project page text and inputs
                var addInp = document.getElementById("projectName");
                addInp.value = "";

                // Chrome has it stored. exit add screen;
                TIMETRACKER.projects.init();
                TIMETRACKER.projects.addingProject = false;
                setTimeout(function(){TIMETRACKER.projects.exitAddScreen(); addNP.innerText = 'Add'; addNP.className = ""; hText.innerText = "Only use underscores or alphanumeric characters.";}, 300);
            });

        }

        //grab project name input and check for validity
        var pNi = document.getElementById("projectName");
        var hText = document.getElementById("helpText");

        if(!pNi.value){
            TIMETRACKER.projects.addingProject = false;
            return false;
        }

        // remove everything but underscores and update input
        var projectName = pNi.value.replace(/\W|[^_\w]/g, '');
        if(!projectName || projectName.length < 2 || projectName.length > 255){
            pNi.className = "error";
            pNi.value = projectName;
            hText.innerText = "Name must be alphanumeric and at least 2 characters.";
            TIMETRACKER.projects.addingProject = false;
            return false;
        }

        if(projectName === pNi.value)
        {
            // Project names match and user is aware of changes
            saveProject(projectName);
            return false;
        }

        TIMETRACKER.projects.addingProject = true;

        pNi.className = "";
        pNi.value = projectName;
        hText.innerText = "Project name was updated.  Click save to apply your changes.";

        // change the button to save
        var addNP = document.getElementById("AddTrackNewProject");
        addNP.innerText = 'Save';
        addNP.className ='pop';
        setTimeout(function(){addNP.className = ''; TIMETRACKER.projects.addingProject = false;}, 800);


    },
    exitAddScreen: function(){

        // remove active timer status
        TIMETRACKER.timer.resetClickEvents();

        var projectList = document.getElementById('projects');
        projectList.value = '';

        // hide the add screen
        var addS = document.getElementById("addProjectScreen");
        addS.className = " miniScreen ";

        // hide the details screen
        var addS = document.getElementById("projectDetailsContainer");
        addS.style.display = "none";

        // show the track screen
        var tb = document.getElementById("trackBoxContainer");
        tb.style.display = "block";

    },
    exeRemovePro: function(idx,name){

        this.removeVerification = true;
        this.removeProject(idx,name);

    },
    removeProject: function(idx,name){

        if(!this.removeVerification)
        {
            var scDetails = document.getElementById("projectDetailsContainer");
            scDetails.innerHTML = "<h2>Delete "+name+" and all associated data?</h2><hr /><p>This action cannot be undone.  Are you sure you want to continue? <br /><br /><a class='cancelConfirm' id='cancelConfirm'>CANCEL</a> <a data-id=''"+name+"' class='deleteConfirm' id='deleteConfirm'>DELETE</a>";
            scDetails.style.display = "block";

            // add event listener to deleteConfirm
            var deleteConfirm = document.getElementById("deleteConfirm");
            var cancelConfirm = document.getElementById("cancelConfirm");
            deleteConfirm.addEventListener("click", function(){ TIMETRACKER.projects.exeRemovePro(idx,name) });
            cancelConfirm.addEventListener("click", function(){ TIMETRACKER.projects.loadProjectDetails(name, idx); });
            return;
        }

        TIMETRACKER.projects.allProjects.splice(idx, 1);
        chrome.storage.sync.set({"projects" : TIMETRACKER.projects.allProjects}, function(){

            // Chrome has it stored. exit add screen;
            TIMETRACKER.projects.init();
            TIMETRACKER.projects.addingProject = false;
            TIMETRACKER.screens.loadHomeScreen();
        });

        // remove data points
        var projectName = "project:" + name;
        projectName = projectName.toString();
        var dp = {};
        dp[projectName] = null;
        chrome.storage.sync.set(dp);

        // stop timer if fired
        if(TIMETRACKER.timer.activeTimer)
            TIMETRACKER.timer.stopTimer();

        // create confirmation screen for next removal
        this.removeVerification = false;

    },
    loadProjectDetails: function(id, projectIndex){
        //show project details
        var scDetails = document.getElementById("projectDetailsContainer");
        scDetails.innerHTML = "";
        scDetails.style.display = "block";

        var prjDetails = TIMETRACKER.projects.allProjects[projectIndex];

        scDetails.innerHTML = "<strong id='projectHours'><span id='totalPHours'>" + prjDetails.totalHours.toFixed(3) + "</span> HOURS TRACKED</strong>";

        // make a remove button
        var rmvProject = document.createElement('span');
        rmvProject.appendChild(document.createTextNode("remove project"));
        rmvProject.title="Remove Project";
        rmvProject.dataset.id = id;
        rmvProject.id = 'removeProject';
        scDetails.appendChild(rmvProject);

        // load the last 25 data points
        var projectString = "project:"+id;
        projectString = projectString.toString();
        chrome.storage.sync.get(projectString, function(obj) {

            TIMETRACKER.projects.loadedProjectData = obj || [];

            if(TIMETRACKER.projects.loadedProjectData[projectString]) {
                var table = '';
                var max = TIMETRACKER.projects.loadedProjectData[projectString].length - 25;
                if(max < 0) max = 0;

                for (var i = TIMETRACKER.projects.loadedProjectData[projectString].length - 1; i >= max; i--) {
                    table += '<tr><td data-time="'+ TIMETRACKER.projects.loadedProjectData[projectString][i].time +'">' + TIMETRACKER.projects.loadedProjectData[projectString][i].date + '</td>' +
                        '<td>' + TIMETRACKER.projects.loadedProjectData[projectString][i].totalTime + ' hours</td></tr>';
                }

                scDetails.innerHTML += "<div class='scrollAbleResults'><table class='dataPoints'>" + table + "</table></div>";
            }

            // add remove project event listener
            var rmvP = document.getElementById("removeProject");
            rmvP.addEventListener("click", function() { TIMETRACKER.projects.removeProject(projectIndex, id);  });

        });

    },
    loadProject: function(id){

        if(this.loadingProject) return false;
        this.loadingProject = true;


        // exit if cannot find project
        var projectIndex = findProjectTitle(id);
        if(projectIndex === false)
        {
            document.getElementById("projectDetailsContainer").innerText = "The project could not be loaded.";
            return;
        }

        // make start button ready
        var strBtn = document.getElementById("startStop");
        strBtn.className = "ready";
        strBtn.dataset.project = id;
        strBtn.innerText = "START";

        this.loadProjectDetails(id,projectIndex);

        // project loaded
        this.loadingProject = false;

    }
};
TIMETRACKER.screens = {
    activeScreen: TIMETRACKER.activeScreen || null,
    activeProject: TIMETRACKER.activeProject || null,
    menuTabs: document.getElementsByClassName("menuLi"),
    allScreens: document.getElementsByClassName("screen"),
    init: function(){

        /* Get active screen  */
        chrome.storage.sync.get("screen", function(obj) {
            TIMETRACKER.screens.activeScreen = obj.screen || 'track';
            TIMETRACKER.screens.loadScreen(TIMETRACKER.screens.activeScreen);
        });

        // Add click listeners
        for(var x = 0; x < this.menuTabs.length; x++)
        {
            this.menuTabs[x].addEventListener('click', function(){
                var tab = this.dataset.tab;
                TIMETRACKER.screens.loadScreen(tab);
            });
        }

    },
    loadHomeScreen: function(){

        // remove active timer status and reset events
        TIMETRACKER.timer.resetClickEvents();

        // reset option list
        var projectList = document.getElementById('projects');
        projectList.value = '';

        //remove details
        var scDetails = document.getElementById("projectDetailsContainer");
        scDetails.style.display = "none";

        // show projects
        var prList = document.getElementById("projects");
        prList.style.display = "inline-block";

        //show timer
        var timerDisplay = document.getElementById("timerDisplay");
        timerDisplay.style.display = "none";


    },
    loadScreen: function(screen)
    {
        TIMETRACKER.screens.activeScreen = screen;
        chrome.storage.sync.set({"screen" : screen}, function(){
            // Chrome has it stored for next browser open
        });
        for(var x = 0; x < this.menuTabs.length; x++)
        {
            this.menuTabs[x].className="menuLi";
            this.allScreens[x].className="screen";
        }
        var activeTab = document.getElementById(screen+"MenuLi");
        var activeScreen = document.getElementById(screen);
        activeTab.className="menuLi active";
        activeScreen.className="screen active";
    }
};

TIMETRACKER.timer = {
    activeTimer: false,
    activeProject: null,
    activeIndex: null,
    startUTC: null,
    timer: 0,
    lastUpdate: null,
    updTInt: null,
    syncMade: false,
    init: function(){

        // setup click listeners
        var tmr = document.getElementById("startStop");
        tmr.addEventListener("click", this, false);
        tmr.addEventListener("touchstart", this, false);

        // check for active timer
        chrome.storage.sync.get("timer", function(obj) {

            //allow timer functions
            TIMETRACKER.timer.syncMade = true;

            if(obj.timer)
            {
                //Timer started
                tmr.className = "stop";
                tmr.innerText = 'STOP';


                // get index to load details
                var projectIndex = findProjectTitle(obj.timer.project);
                if(projectIndex === false)
                {
                    document.getElementById("projectDetailsContainer").innerText = "The project could not be loaded.";
                    return;
                }

                // show timer active icon
                chrome.browserAction.setIcon({path: "images/icon19_active.png"});

                // add project index into object property
                TIMETRACKER.timer.activeIndex = projectIndex;

                // load details
                TIMETRACKER.projects.loadProjectDetails(obj.timer.project, projectIndex);

                // start timer functions
                TIMETRACKER.timer.activeTimer = true;
                TIMETRACKER.timer.activeProject = obj.timer.project;
                TIMETRACKER.timer.startUTC = obj.timer.startTime;
                TIMETRACKER.timer.updateTimer();

                // update timer every 500 ms  (1/2 second)
                TIMETRACKER.timer.updTInt = setInterval(function(){TIMETRACKER.timer.updateTimer();},500);

                //hide project list
                var prList = document.getElementById("projects");
                prList.style.display = "none";

                //show timer
                var timerDisplay = document.getElementById("timerDisplay");
                timerDisplay.style.display = "block";


            }else{
                // show timer not-active icon
                chrome.browserAction.setIcon({path: "images/icon19.png"});
            }
        });

    },
    handleEvent: function(e){
        if(!this.syncMade) return false;
        if(e.type === 'click' || e.type === 'touchstart')
        {
            if (this.activeTimer)  this.stopTimer();
            else this.startTimer();
        }
    },
    resetClickEvents: function(){
        //reset click listener to listen for stop events
        var tmr = document.getElementById("startStop");
        tmr.className = "";
        tmr.dataset.project = '';
    },
    updateTimer: function(){

        var timeNow = new Date().getTime();

        // timer in seconds
        var updatedTimer = parseFloat(Math.round(((timeNow - this.startUTC)/1000) * 100) / 100).toFixed(2);
        var hoursTime = (updatedTimer / 3600).toFixed(3);

        var hours, seconds, minutes;

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        if(updatedTimer < 60)
        {
            seconds = updatedTimer;
            if(seconds < 10) seconds = '0'+seconds;
            updatedTimer = '0:00:' + seconds;
        }else if(updatedTimer > 60 && updatedTimer < 3600){
            // less than one hour
            seconds = parseFloat(Math.round((updatedTimer % 60) * 100) / 100).toFixed(2);
            if(seconds < 10) seconds = '0'+seconds;
            minutes = Math.floor(updatedTimer / 60).toFixed(0);
            minutes = pad(minutes,2);
            updatedTimer = '0:'+minutes+':'+seconds;
        }else{
            seconds = parseFloat(Math.round((updatedTimer % 60) * 100) / 100).toFixed(2);
            if(seconds < 10) seconds = '0'+seconds;
            hours = Math.floor(updatedTimer / 3600).toFixed(0);
            minutes = Math.floor((updatedTimer / 60) % 60).toFixed(0);
            minutes = pad(minutes,2);
            updatedTimer = hours+':'+minutes+':'+seconds;
        }

        // update timer
        var tmr = document.getElementById("timerValue");
        tmr.innerText = updatedTimer;

        var tHr = document.getElementById("totalPHours");
        if(tHr.innerText)
        {
            var hoursWorked = parseFloat(TIMETRACKER.projects.allProjects[this.activeIndex].totalHours);
            hoursWorked += parseFloat(hoursTime);
            tHr.innerText = hoursWorked.toFixed(3).toString();
        }

    },
    startTimer: function(){

        if(this.activeTimer) return false;

        this.startUTC = new Date().getTime();

        var tmr = document.getElementById("startStop");

        // if no project - dont start timer
        if(!tmr.dataset.project) return false;

        //update project index
        var projectIndex = findProjectTitle(tmr.dataset.project);
        if(projectIndex === false)
        {
            document.getElementById("projectDetailsContainer").innerText = "The project could not be loaded.";
            return;
        }
        this.activeIndex = projectIndex;


        // update class and text for action button
        tmr.className = "stop";
        tmr.innerText = 'STOP';


        var tmrv = document.getElementById("timerValue");
        tmrv.innerText = '0:00:00';


        // activate project and timer
        this.activeProject = tmr.dataset.project;
        this.activeTimer = true;

        //sync and save new timer
        chrome.storage.sync.set({"timer" : { 'startTime' : this.startUTC, 'project' : this.activeProject }}, function() {
            // saved
        });

        // update icon
        chrome.browserAction.setIcon({path: "images/icon19_active.png"});

        // update timer every 500 ms (1/2 second)
        TIMETRACKER.timer.updTInt = setInterval(function(){TIMETRACKER.timer.updateTimer();}, 500);

        //hide project list
        var prList = document.getElementById("projects");
        prList.style.display = "none";

        //show timer
        var timerDisplay = document.getElementById("timerDisplay");
        timerDisplay.style.display = "block";


    },
    stopTimer: function(){

        // no timer to stop
        if(!this.activeTimer) return false;

        // get stop time
        var date = new Date();
        var stopTime = date.getTime();
        var amPm = null;

        var month = date.getMonth().toFixed(0);
        month++;

        var time = date.getHours();
        if(time > 12){
            time -= 12;
            amPm = 'pm';
        }else{
            amPm = 'am';
        }

        var minutes = date.getMinutes();
        if(minutes < 10) minutes = "0"+minutes;

        time = time + ":"+minutes  + " " +amPm;

        var totalMin = ((stopTime -  this.startUTC) / 60000).toFixed(1);

        var fullDate = month + "-" + date.getDate() + "-" + date.getFullYear();
        var fullTime = " " + time + "  (" + totalMin + " min)";

        chrome.browserAction.setIcon({path: "images/icon19.png"});

        chrome.storage.sync.get("timer", function(obj){
            if(obj.timer)
            {
                var timeSpent = stopTime - obj.timer.startTime;

                var project = obj.timer.project;

                var hoursSpent = timeSpent / 3600000;
                hoursSpent = hoursSpent.toFixed(4);

                // check for index position array to get data about project
                var projectIndex = findProjectTitle(obj.timer.project);
                if(projectIndex === false)
                {
                    // not a project timer - may be timer set w/o a project
                    clearInterval(TIMETRACKER.timer.updTInt);
                    TIMETRACKER.projects.activeProject = false;
                    TIMETRACKER.projects.loadedProjectData = null;

                    chrome.storage.sync.set({"timer" : null}, function() {
                        TIMETRACKER.timer.activeTimer = false;
                    });

                    TIMETRACKER.screens.loadHomeScreen();
                    return;
                }

                // update hours spent on project
                TIMETRACKER.projects.allProjects[projectIndex].totalHours += parseFloat(hoursSpent);
                chrome.storage.sync.set({"projects" : TIMETRACKER.projects.allProjects });

                // store data point with project name
                var projectName = "project:" + project;
                projectName = projectName.toString();

                chrome.storage.sync.get(projectName, function(obj){

                    // create data point
                    var dp = {};
                    if(obj[projectName] && obj[projectName].length > 0)
                    {
                        obj[projectName].push({
                            date : fullDate,
                            totalTime : hoursSpent,
                            time: fullTime
                        });
                        dp[projectName] = obj[projectName];
                    }else{
                        dp[projectName] = [ {
                            date : fullDate,
                            totalTime : hoursSpent,
                            time: fullTime
                        } ];
                    }

                    chrome.storage.sync.set(dp, function(){

                        clearInterval(TIMETRACKER.timer.updTInt);
                        TIMETRACKER.screens.loadHomeScreen();
                        TIMETRACKER.timer.activeIndex = null;

                        var tmr = document.getElementById("startStop");
                        tmr.className = "ready";
                        tmr.innerText = 'START';


                        // select and load project
                        document.getElementById("projects").value = project;
                        TIMETRACKER.projects.loadProject(project);

                    });

                });


                chrome.storage.sync.set({"timer" : null}, function() {
                    TIMETRACKER.timer.activeTimer = false;
                });

            }
        });
    }

};

TIMETRACKER.goals = {
    allGoals: null,
    finishedGoals: null,
    addingGoal: false,
    finalizingGoal: false,
    init: function(){

        // set listener for add goal
        var adBtn = document.getElementById("addButton");
        adBtn.addEventListener("click", this, false);
        adBtn.addEventListener("touchstart", this, false);

        // add listener for exiting the goal screen
        var exitBtn = document.getElementById("exitAddGoal");
        exitBtn.addEventListener("click", this);
        exitBtn.addEventListener("touchstart", this, false);

        // add listener to finalize add goal
        var addGoalBtn = document.getElementById("addNewGoal");
        addGoalBtn.addEventListener("click", this, false);
        addGoalBtn.addEventListener("touchstart", this, false);

        // load goals to screen
        this.loadGoals();

    },
    handleEvent: function(e){
        if(e.type === 'click' || e.type === 'touchstart')
        {
            if(e.toElement.className === 'addPlus' || e.toElement.className === 'addGBtn')
            {
                this.addGoal();
            }
            if(e.toElement.className === 'exitMiniGoals')
            {
                this.exitGoalScreen();
            }
            if(e.toElement.id === 'addNewGoal')
            {
                this.finalizeAddGoal();
            }
            if(e.toElement.className === 'goalAchieved'){
                this.goalAchieved(e.toElement.dataset.goal);
            }

        }
    },
    loadGoals: function(){

        var goalsContainer = document.getElementById("showGoals");
        goalsContainer.innerHTML = "";
        goalsContainer.style.display = "block";

        /* Load Finished goals from local or add new blank array  */
        chrome.storage.sync.get("finished_goals", function(obj) {

            // set the storage returned var in the object for use later or set as empty array
            TIMETRACKER.goals.finishedGoals = obj.finished_goals || [];

        });


        /* Load Goals from local or add new blank array  */
        chrome.storage.sync.get("goals", function(obj) {

            // set the storage returned var in the object for use later or set as empty array
            TIMETRACKER.goals.allGoals = obj.goals || [];

            // SHOW FINISHED Goals
            goalsContainer.innerHTML = "<div id='finished_goals'><span id='num_finished'>" + TIMETRACKER.goals.finishedGoals.length + "</span> GOALS FINISHED</div>";

            var table = '';
            var max = TIMETRACKER.goals.allGoals.length - 50;
            if(max < 0) max = 0;

            for (var i = TIMETRACKER.goals.allGoals.length - 1; i >= max; i--) {
                table += '<tr><td data-goal="'+i+'" class="goalAchieved">&#9989;</td><td data-time="'+ TIMETRACKER.goals.allGoals[i].time +'">' + TIMETRACKER.goals.allGoals[i].date + '</td>' +
                    '<td>' + TIMETRACKER.goals.allGoals[i].name + ' </td></tr>';
            }

            goalsContainer.innerHTML += "<table class='goals'>" + table + "</table>";

            var goalsListeners = document.getElementsByClassName("goalAchieved");
            for(var x = 0; x < goalsListeners.length; x++)
            {

                goalsListeners[x].addEventListener("click", TIMETRACKER.goals, false);
                goalsListeners[x].addEventListener("touchend", TIMETRACKER.goals, false);

            }

        });

    },
    goalAchieved: function(index){

        var goalAch = this.allGoals[index];

        TIMETRACKER.goals.finishedGoals.push(goalAch);

        this.allGoals.splice(index, 1);
        chrome.storage.sync.set({"goals" : TIMETRACKER.goals.allGoals}, function(){
            TIMETRACKER.goals.loadGoals();
        });
        chrome.storage.sync.set({"finished_goals" : TIMETRACKER.goals.finishedGoals}, function(){
            TIMETRACKER.goals.loadGoals();
        });



    },
    addGoal: function() {
        if (this.addingGoal) return false;
        this.addingGoal = true;
        var adBtn = document.getElementById("addButton");
        adBtn.className = "fadeOut";

        var adBtn = document.getElementById("showGoals");
        adBtn.className = "fadeOut";

        var addGoalScreen = document.getElementById("addGoalScreen");
        addGoalScreen.style.display = "block";
        setTimeout(function(){ addGoalScreen.className = "miniScreen show"; }, 30);


        // add enter listener
        var pNi = document.getElementById("goalName");
        pNi.addEventListener('keypress', function (e) {
            var key = e.which || e.keyCode;
            if (key === 13) { // 13 is enter
                e.preventDefault();
                TIMETRACKER.goals.finalizeAddGoal();
            }
        });

    },
    saveGoal: function(name)
    {
        var GhText = document.getElementById("GHtext");
        var goal = document.getElementById("goalName");

        if (this.allGoals.length > 50)
        {
            GhText.innerText = "Too many goals overachiever, check some off first.";
            return false;
        }

        for(var i = 0; i < this.allGoals.length; i++)
        {
            if(this.allGoals[i].name === name)
            {
                // Force exit // reset addingProject variable for adding new project
                this.finalizingGoal = false;
                GhText.innerText = "You already have a goal with the name "+name+".";
                return false;
            }
        }

        // trigger css animation
        var addNP = document.getElementById("addNewGoal");
        addNP.className = "save";
        goal.className = "";

        GhText.innerText = "Saving goal.";

        // get  time
        var date = new Date();
        var stopTime = date.getTime();
        var amPm = null;

        var month = date.getMonth().toFixed(0);
        month++;

        var time = date.getHours();
        if(time > 12){
            time -= 12;
            amPm = 'pm';
        }else{
            amPm = 'am';
        }

        var minutes = date.getMinutes();
        if(minutes < 10) minutes = "0"+minutes;

        time = time + ":"+minutes  + " " +amPm;

        var totalMin = ((stopTime -  this.startUTC) / 60000).toFixed(1);

        var fullDate = month + "-" + date.getDate() + "-" + date.getFullYear();
        var fullTime = " " + time + "  (" + totalMin + " min)";

        // save project to local storage and google sync
        TIMETRACKER.goals.allGoals.push( { name: name, finished : false, date : fullDate, time: fullTime  } );
        chrome.storage.sync.set({"goals" :  TIMETRACKER.goals.allGoals }, function(){

            // clear add project page text and inputs
            var addInp = document.getElementById("goalName");
            addInp.value = "";

            // Chrome has it stored. exit add screen;
            TIMETRACKER.goals.init();
            TIMETRACKER.goals.finalizingGoal = false;
            setTimeout(function(){TIMETRACKER.goals.exitGoalScreen(); addNP.innerText = 'Add'; addNP.className = ""; GhText.innerText = "Try and keep your goals short and sweet.";}, 300);
        });



    },
    finalizeAddGoal: function(){
        if(this.finalizingGoal) return false;
        this.finalizingGoal = true;

        var goal = document.getElementById("goalName");

        // remove everything but underscores and update input
        var goalName = goal.value.replace(/[^.:+,!@#$%&*();/?= _\w-]/g, '');
        if(!goalName || goalName.length < 2 || goalName.length > 300){
            goal.className = "error";
            goal.value = goalName;
            this.finalizingGoal = false;
            return false;
        }

        if(goalName === goal.value)
        {
            // Project names match and user is aware of changes
            this.saveGoal(goalName);
            return false;
        }

        goal.className = "";
        goal.value = goalName;

        // change the button to save
        var addNP = document.getElementById("addNewGoal");
        addNP.innerText = 'Save';
        addNP.className ='pop';
        setTimeout(function(){addNP.className = ''; TIMETRACKER.goals.finalizingGoal = false;}, 800);


    },
    exitGoalScreen: function(){
        this.addingGoal = false;
        var adBtn = document.getElementById("addButton");
        adBtn.className = "addGBtn";
        var addGoalScreen = document.getElementById("addGoalScreen");
        addGoalScreen.className = "miniScreen";
        addGoalScreen.style.display = "none";

        var adBtn = document.getElementById("showGoals");
        adBtn.className = "";

    }



};



// find project object and pull details
function findProjectTitle(name)
{
    for(var i = 0; i < TIMETRACKER.projects.allProjects.length; i++)
    {
        if(TIMETRACKER.projects.allProjects[i].name === name)
        {
            return i;
        }
    }
    return false;
}

/**
 * Listen for the DOMContentLoaded event and fire initial actions and screens
 *
 * @param {function(string)} callback - called when the extension is clicked.
 *
 **/
document.addEventListener('DOMContentLoaded', function() {

    TIMETRACKER.main.start();

});
