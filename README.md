# WebDev Time Tracker #
Hello all you web developers out there.  If you are like me, you want to eliminate paper and extra work.  The WebDev Time Tracker allows you to track your time spent on projects right in your browser, where you work most.  It makes life super easy when keeping track of billable hours. Plus, there is a goals section to create and complete your tasks.



### How do I get set up? ###

The Chrome Extension will be packaged and released through the Chrome store.  This repo will serve as a way to improve on the codebase. 